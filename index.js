var request = require('request');
const options = require('./media-payload');

const callRequest = (i) => new Promise((resolve, reject) => {
    request(options, function (error, response, body) {
        if (error) {
            console.log('error', error);
            return reject(error);
        }
        if (body[0]) {
            return resolve(body);
        }
        return resolve(body);
    });
})

const sleep = (ms) => {
    return new Promise((resolve) => {
        setTimeout(resolve, ms)
    })
}

const f = async (loop) => {
    for (let i = 0; i < loop; i++) {
        await sleep(300);
        callRequest(i).then(data => {
            console.log(i, 'data', data);
        })
    }
}
f(200);