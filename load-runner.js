const loadtest = require("loadtest");
const data = require("./media-payload");
const { argv } = process
const testNumber = argv[2] - 1
console.log(testNumber)
const runnerCollection = [
    {
        concurrency: 1,
        requestsPerSecond: 1,
        maxSeconds: 1,
        maxRequests: 1,
    },
    {//
        concurrency: 1,
        requestsPerSecond: 1,
        maxSeconds: 5,
        maxRequests: 20,
    },
    {//2
        concurrency: 5,
        requestsPerSecond: 10,
        maxSeconds: 10,
        maxRequests: 100,
    },
    {//3
        concurrency: 10,
        requestsPerSecond: 20,
        maxSeconds: 10,
        maxRequests: 100,
    },
    {//4
        concurrency: 25,
        requestsPerSecond: 16,
        maxSeconds: 10,
        maxRequests: 250,
    },
    {//5
        concurrency: 50,
        requestsPerSecond: 16,
        maxSeconds: 10,
        maxRequests: 500,
    },
    {//5
        concurrency: 50,
        requestsPerSecond: 16,
        maxSeconds: 200,
        maxRequests: 10000,
    }
]
console.log(runnerCollection[testNumber])

const options = {
    ...runnerCollection[testNumber],
    url: data.url + data.queryString,
    method: data.method,
    body: "",
    headers: data.headers,
    requestGenerator: (params, options, client, callback) => {
        const message = JSON.stringify(data.body);
        params = data.qs;
        options.headers["Content-Type"] = "application/json";
        const request = client(options, callback);
        request.write(message);
        return request;
    }
};
if (runnerCollection[testNumber])
    loadtest.loadTest(options, (error, results) => {
        if (error) {
            return console.error("Got an error: %s", error);
        }
        console.log(results);
        console.log("Tests run successfully");
    });
